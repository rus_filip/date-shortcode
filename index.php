<?php 
/*
Plugin Name: Date Tool
Plugin URI: 
Description: A simple plugin that returns the current date.
*/

function datetool_current($args)
{
	global $wp_query;
	
	$output = "format";
	extract(shortcode_atts(array(
		'format' => 'js F Y ',
	), $args));
	
	return date($format);
}
add_shortcode('datetool_current', 'datetool_current');
?>